const gulp = require('gulp')
const rollup = require('rollup');
const commonjs = require('@rollup/plugin-commonjs');
const ts = require('@rollup/plugin-typescript');

const screeps = require('gulp-screeps');
const credentials = require('./credentials.js');

function build() {
   return rollup
      .rollup({
         input: "src/main.ts",
         plugins: [
            commonjs({}),
            ts({
               // module: "esnext",
               // moduleResolution: "node",
               // allowUmdGlobalAccess: true,
               // types: ["lodash"]
            })
         ],
      })
      .then(bundle => {
         return bundle.write({
            dir: "dist",
            format: "cjs",
            sourcemap: true,
         })
      })
}

function upload() {
   return gulp.src("dist/*")
      .pipe(screeps(credentials))
}

gulp.task(build)

exports.default = gulp.series(build, upload)
